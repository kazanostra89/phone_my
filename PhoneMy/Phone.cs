﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneMy
{
    class Phone
    {
        private string name;
        private string model;
        private List<PhoneProgram> programs;

        public Phone(string name, string model)
        {
            this.name = name;
            this.model = model;
            programs = new List<PhoneProgram>();
        }

        public void InstallProgram(PhoneProgram program)
        {
            programs.Add(program);
        }

        public void DeletedProgram(string nameProgram)
        {
            programs.Remove(programs.Find(item => item.Name == nameProgram));
        }

        public void RunProgram(string nameProgram)
        {
            programs.Find(item => item.Name == nameProgram).Run();
        }

        public bool CheckNameForPhoneProgram(string nameProgram)
        {
            return programs.Exists(item => item.Name == nameProgram);
        }

        public void PrintInfoPhone()
        {
            Console.WriteLine("Имя телефона: " + name + ", модель: " + model);
        }

        public void PrintInfoListPrograms()
        {
            if (programs.Count == 0)
            {
                Console.WriteLine("Список установленных программ пуст!");
                return;
            }

            Console.WriteLine("{0,-3}{1,-7}", "№", "Name");

            for (int i = 0; i < programs.Count; i++)
            {
                Console.WriteLine("{0,-3}{1,-7}", (i + 1), programs[i].Name);
            }

            Console.WriteLine();
        }

        public int CountPhoneProgram
        {
            get { return programs.Count; }
        }

    }
}
