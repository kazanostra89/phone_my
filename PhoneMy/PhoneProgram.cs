﻿using System;

namespace PhoneMy
{
    abstract class PhoneProgram
    {
        protected string name;

        public PhoneProgram(string name)
        {
            this.name = name;
        }

        public abstract void Run();

        public abstract string Name { get; }
    }
}
