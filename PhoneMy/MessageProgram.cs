﻿using System;

namespace PhoneMy
{
    class MessageProgram : PhoneProgram
    {
        public MessageProgram()
            : base("Message")
        {

        }

        public override void Run()
        {
            Console.WriteLine("Runing program " + name + "!");
        }

        public override string Name
        {
            get { return name; }
        }
    }
}
