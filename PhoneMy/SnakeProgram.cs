﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneMy
{
    class SnakeProgram : PhoneProgram
    {
        public SnakeProgram()
            : base("Snake")
        {

        }
        
        public override void Run()
        {
            Console.WriteLine("Runing program " + name + "!");
        }

        public override string Name
        {
            get { return name; }
        }
    }
}
