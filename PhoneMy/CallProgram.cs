﻿using System;

namespace PhoneMy
{
    class CallProgram : PhoneProgram
    {
        public CallProgram()
            : base("Call")
        {

        }

        public override void Run()
        {
            Console.WriteLine("Runing program " + name + "!");
        }

        public override string Name
        {
            get { return name; }
        }
    }
}
