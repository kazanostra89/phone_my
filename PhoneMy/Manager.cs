﻿using System;

namespace PhoneMy
{
    class Manager
    {
        private Phone phone;

        public Manager()
        {
            phone = new PhoneMy.Phone("NOKIA", "8800");
        }

        public void Start()
        {
            bool start = true;
            int selectMenu;
            string nameProgram;

            while (start)
            {
                Console.Clear();
                phone.PrintInfoPhone();
                Console.WriteLine();
                PrintMainMenu();

                selectMenu = InputSelectMenu(0, 3, "Выбирете пункт меню: ");

                switch (selectMenu)
                {
                    case 1:
                        Console.Clear();
                        phone.PrintInfoListPrograms();
                        PrintInstallMenu();

                        selectMenu = InputSelectMenu(1, 3, "Выбирите доступную для установки программу: ");

                        switch (selectMenu)
                        {
                            case 1:
                                if (phone.CheckNameForPhoneProgram("Snake"))
                                {
                                    Console.WriteLine("Программа уже установленна!\nОперация прервана!");
                                    break;
                                }
                                else
                                {
                                    phone.InstallProgram(new SnakeProgram());
                                }
                                break;
                            case 2:
                                if (phone.CheckNameForPhoneProgram("Call"))
                                {
                                    Console.WriteLine("Программа уже установленна!\nОперация прервана!");
                                    break;
                                }
                                else
                                {
                                    phone.InstallProgram(new CallProgram());
                                }
                                break;
                            case 3:
                                if (phone.CheckNameForPhoneProgram("Message"))
                                {
                                    Console.WriteLine("Программа уже установленна!\nОперация прервана!");
                                    break;
                                }
                                else
                                {
                                    phone.InstallProgram(new MessageProgram());
                                }
                                break;
                        }
                        Console.ReadKey();
                        break;
                    case 2:
                        if (phone.CountPhoneProgram == 0)
                        {
                            break;
                        }

                        PrintSupportingInfo();

                        nameProgram = InputStringNameProgram("Введите имя удаляемой программы: ");

                        phone.DeletedProgram(nameProgram);

                        Console.WriteLine("Программа " + nameProgram + " успешно удалена!");

                        Console.ReadKey();
                        break;
                    case 3:
                        if (phone.CountPhoneProgram == 0)
                        {
                            break;
                        }

                        PrintSupportingInfo();

                        nameProgram = InputStringNameProgram("Введите имя запускаемой программы: ");

                        phone.RunProgram(nameProgram);

                        Console.ReadKey();
                        break;
                    case 0:
                        start = false;
                        break;
                }

            }
        }


        private int InputSelectMenu(int min, int max, string message)
        {
            int number;
            bool check;

            do
            {
                Console.Write(message);
                check = int.TryParse(Console.ReadLine(), out number);

            } while (check == false || number < min || number > max);

            return number;
        }

        private string InputStringNameProgram(string message)
        {
            string nameProgram;

            do
            {
                Console.Write(message);
                nameProgram = Console.ReadLine();

            } while (phone.CheckNameForPhoneProgram(nameProgram) != true);

            return nameProgram;
        }

        private void PrintMainMenu()
        {
            Console.WriteLine("1. Установить программу");
            Console.WriteLine("2. Удалить программу");
            Console.WriteLine("3. Запустить программу");
            Console.WriteLine("0. Выход");
        }

        private void PrintInstallMenu()
        {
            Console.WriteLine("1. Установить змейку");
            Console.WriteLine("2. Установить програм. для звонков");
            Console.WriteLine("3. Установить програм. для сообщений");
        }

        private void PrintSupportingInfo()
        {
            Console.Clear();
            phone.PrintInfoListPrograms();
        }

    }
}
